<?php

namespace App\Repositories\Eloquent;

use App\Models\Item;
use App\Repositories\Interfaces\ItemRepositoryInterface;
use Illuminate\Database\Eloquent\Collection;

class ItemRepository extends BaseRepository implements ItemRepositoryInterface
{
    public function __construct()
    {
        parent::__construct(new Item());
    }

    public function viewable() :?Collection
    {
        return $this->model->where('viewable','YES')->get();
    }

    public function getViewableByProduct($product_id) :?Collection
    {
        return $this->model->where('viewable','YES')
        ->where('product_id',$product_id)->get();
    }

    public function getByProduct($product_id) :?Collection
    {
        return $this->model->where('product_id',$product_id)->get();
    }
}
