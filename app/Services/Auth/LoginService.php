<?php
namespace App\Services\Auth;

use App\Http\Resources\Representation\RepresentationLoginResource;
use App\Repositories\Eloquent\Auth\LoginRepository;
use App\Lib\ResponseTemplate;
use App\Repositories\Eloquent\RepresentationDetailRepository;
use Illuminate\Http\Request;

class LoginService extends ResponseTemplate{

    protected $loginRepository;

    public function __construct(LoginRepository $loginRepository)
    {
        $this->loginRepository = $loginRepository;
    }

    public function RepresentationLoginPage(Request $request)
    {
        $detailRepository = new RepresentationDetailRepository($request->domain);
        if($representation = $detailRepository->findBYDomain() ?? NULL)
        {
            $this->setData(new RepresentationLoginResource($representation));
        }
        else
        {
            $this->setStatus(410);
        }
        return $this->response();
    }

    public function RepresentationLogin(Request $request)
    {
        if($account = $this->loginRepository->checkPassword($request->phone,$request->password))
        {
            // $user = $this->loginRepository->updateToken($user);
            if($account->user->representation->domain == $request->domain)
            $this->setData($account);
             else
            $this->setStatus(403);
        }
        else
        {
            $this->setErrors(['email' => ['موبایل یا پسورد اشتباه است']]);
            $this->setStatus(401);
        }

        return $this->response();
    }

    public function adminLogin(Request $request)
    {
        if($account = $this->loginRepository->checkPassword($request->phone,$request->password))
        {
            // $user = $this->loginRepository->updateToken($user);
            if($account->admin)
            {
            $this->setData($account);
            }
             else
             {

            $this->setStatus(401);
                $this->setErrors(['email' => ['موبایل یا پسورد اشتباه است']]);
             }
        }
        else
        {
            $this->setErrors(['email' => ['موبایل یا پسورد اشتباه است']]);
            $this->setStatus(401);
        }

        return $this->response();
    }

    public function executerLogin(Request $request)
    {
        if ($account = $this->loginRepository->checkPassword($request->phone, $request->password)) {
            // $user = $this->loginRepository->updateToken($user);
            if ($account->executer) {
                $this->setData($account);
            } else {

                $this->setStatus(401);
                $this->setErrors(['email' => ['موبایل یا پسورد اشتباه است']]);
            }
        } else {
            $this->setErrors(['email' => ['موبایل یا پسورد اشتباه است']]);
            $this->setStatus(401);
        }

        return $this->response();
    }
}
