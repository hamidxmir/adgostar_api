<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateRepresentationsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('representations', function (Blueprint $table) {
            $table->id();
            $table->unsignedBigInteger('account_id');
            $table->foreign('account_id')->references('id')
            ->on('accounts')->onDelete('cascade');
            $table->unsignedBigInteger('parent_id')->nullable();
            $table->foreign('parent_id')->references('id')
            ->on('representations')->onDelete('cascade');
            $table->decimal('balance',20,0)->default(0);
            $table->decimal('removable',20,0)->default(0);
            $table->string('domain')->nullable();
            $table->enum('status',['UNPAID','PAID','COMPLETED','PENDING','ACTIVE','SUSPENDED'])->default('UNPAID');
            $table->enum('kind',['NORMAL','SENIOR']);
            $table->string('api_token',80);
            $table->softDeletes();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('representations');
    }
}
