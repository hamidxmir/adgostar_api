<?php

namespace Database\Seeders;

use App\Models\Account;
use Illuminate\Database\Seeder;

class AdminSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $AdminAccount = Account::create([
            'firstName' => 'رضا',
            'lastName' => 'حسینی',
            'phone' => '09124578694',
            'password' => '12345678',
        ]);
        $specialRepresentation = $AdminAccount->representation()->create([
            'title' => 'ادگستر',
            'logo' => '/media/images/representation/logo/adgostar.jpg',
            'domain' => '127.0.0.1:8001',
            'kind' => 'SPECIAL'
         ]);
        $specialRepresentation->users()->create(['account_id' => $AdminAccount->id]);
        $AdminAccount->admin()->create([]);
        $AdminAccount->executer()->create([]);
    }
}
