<?php

namespace Database\Seeders;

use App\Models\Account;
use App\Models\Executer;
use Illuminate\Database\Seeder;

class ExecuterSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $accountA = Account::create([
            'firstName' => 'رضا',
            'lastName' => 'حسینی',
            'phone' => '09124578694',
            'password' => '12345678',
        ]);
        $accountA->executer()->create([]);
    }
}
