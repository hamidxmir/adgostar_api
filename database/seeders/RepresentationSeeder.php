<?php

namespace Database\Seeders;

use App\Models\Account;
use Illuminate\Database\Seeder;

class RepresentationSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {

       $accountA = Account::create([
            'firstName' => 'حسین',
            'lastName' => 'تقوی',
            'phone' => '09165784875',
            'password' => '12345678',
        ]);
        $specialRepresentation = $accountA->representation()->create([
           'title' => 'شاهین',
           'logo' => '/media/images/representation/logo/shahin.jpg',
           'domain' => '127.0.0.1:8002',
           'kind' => 'SPECIAL'
        ]);
        $specialRepresentation->users()->create(['account_id' => $accountA->id]);
        $accountB =  Account::create([
            'firstName' => 'محمد',
            'lastName' => 'زاهدی',
            'phone' => '09335658475',
            'password' => '12345678',
        ]);
        $NormalRepresentation = $accountB->representation()->create([
            'title' => 'سفیر',
            'logo' => '/media/images/representation/logo/safir.jpg',
            'domain' => '127.0.0.1:8003',
            'kind' => 'NORMAL',
            'parent_id' => $specialRepresentation->id
         ]);
         $NormalRepresentation->users()->create(['account_id' => $accountB->id]);
    }
}
