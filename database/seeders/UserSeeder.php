<?php

namespace Database\Seeders;

use App\Models\Account;
use App\Models\Representation;
use App\Models\User;
use Illuminate\Database\Seeder;

class UserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $normalRepresentation = Representation::where('kind','NORMAL')->first();
        $specialRepresentation = Representation::where('kind','SPECIAL')->first();
        $accountA = Account::create([
            'firstName' => 'رضا',
            'lastName' => 'حسینی',
            'phone' => '09124578694',
            'password' => '12345678',
        ]);
        $accountB = Account::create([
            'firstName' => 'اشکان',
            'lastName' => 'داوودی',
            'phone' => '09397845769',
            'password' => '12345678',
        ]);
        $accountC = Account::create([
            'firstName' => 'محسن',
            'lastName' => 'رضایی',
            'phone' => '09135478968',
            'password' => '12345678',
        ]);
        $accountD = Account::create([
            'firstName' => 'محمد',
            'lastName' => 'نوروزی',
            'phone' => '09174785986',
            'password' => '12345678',
        ]);
        $accountE = Account::create([
            'firstName' => 'کوروش',
            'lastName' => 'طهماسبی',
            'phone' => '09145789868',
            'password' => '12345678',
        ]);
        $normalRepresentation->users()->create(['account_id' => $accountA->id]);
        $normalRepresentation->users()->create(['account_id' => $accountB->id]);
        $specialRepresentation->users()->create(['account_id' => $accountC->id]);
        $specialRepresentation->users()->create(['account_id' => $accountD->id]);
        $specialRepresentation->users()->create(['account_id' => $accountE->id]);




    }
}
