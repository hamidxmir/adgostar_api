<?php

use App\Http\Controllers\Representation\Agent\DetailController;
use Illuminate\Support\Facades\Route;

Route::resource('login', 'Auth\LoginController');
Route::get('representationDetail',[DetailController::class,'index']);
Route::group(['prefix' => 'agent','middleware' => 'auth:representation'], function () {
    Route::resource('detail','DetailController');
    Route::resource('users','Agent\UserController');
    Route::resource('subsets','Agent\SubsetController');
    Route::resource('userTickets','Agent\UserTicketController');
    Route::resource('userTickets.messages','Agent\UserTicketMessageController');
    Route::resource('subsetTickets','Agent\SubsetTicketController');
    Route::resource('subsetTickets.messages','Agent\SubsetTicketMessageController');
    Route::resource('tickets','Agent\TicketController');
    Route::resource('tickets.messages','Agent\TicketMessageController');
    Route::resource('products', 'Agent\ProductController');
    Route::resource('items.periods', 'Agent\ItemPeriodController')->shallow();
});
Route::group(['prefix' => 'user','middleware' => 'auth:user'], function () {
    Route::resource('detail','DetailController');
    Route::resource('tickets', 'User\TicketController');
    Route::resource('tickets.messages','User\TicketMessageController');
});
Route::resource('categories', 'CategoryController');
Route::resource('services', 'ServiceController');
Route::get('instagram-validation',function(){
    echo file_get_contents('https://instagram.com/hamidrezakiani_official');
});

